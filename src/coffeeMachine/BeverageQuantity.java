package coffeeMachine;

import java.util.HashMap;
import java.util.Map;

public class BeverageQuantity implements BeverageQuantityChecker {

	public static final int MAX_LEVEL = 100;
	
	private Map<String,Integer> quantity;
	
	public BeverageQuantity() {
		this.quantity = new HashMap<>();
	}
	
	@Override
	public boolean isEmpty(String drink) {
		DrinkType drinkType = DrinkType.valueOf(drink);
		if(!this.quantity.containsKey(drinkType.type))
			return true;
		if(DrinkType.ORANGE.type.equals(drinkType.type)) {
			return this.quantity.get(drinkType.type) < 0;
		} 
		return !((this.quantity.get(drinkType.type) - drinkType.quantity) >= 0 && (this.quantity.get(DrinkType.WATER.type) - drinkType.waterQuantity ) >= 0); 
	}
	
	public void provide(DrinkType drinkType) {
		this.quantity.put(drinkType.type,this.quantity.get(drinkType.type) - drinkType.quantity);
		this.quantity.put(DrinkType.WATER.type,this.quantity.get(DrinkType.WATER.type) - drinkType.waterQuantity);

	}

	public void refill(String type,int newQuantity) {
		if(newQuantity > MAX_LEVEL) {
			newQuantity = MAX_LEVEL;
		} 
		this.quantity.put(type, newQuantity);
	}
	
	public void print() {
		System.out.println("\nMachine Beverage Quantity : \n");
		for(Map.Entry<String, Integer> drinkQuantity : quantity.entrySet()) {
			System.out.println("\n"+drinkQuantity.getKey() + " : " + drinkQuantity.getValue());
		}
	}
}
