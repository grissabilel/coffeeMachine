package coffeeMachine;

public class DrinkCold extends Drink{

	public DrinkCold(DrinkType drinkType) {
		super(drinkType);
	}

	@Override
	public String extra() {
		return "";
	}

}
