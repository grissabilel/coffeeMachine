package coffeeMachine;

public enum DrinkType {
	
	THE("T",0.4f,2,5),
	COFFEE("C",0.6f,3,3),
	CHOCOLATE("H",0.5f,5,1),
	ORANGE("O",0.6f,10,0),
	WATER("W",0.0f,10,0);
	
	String type;
	//How match quantity the drink need
	int quantity;
	//How match water the drink need
	int waterQuantity;
	float price;
	
	DrinkType(String type,float price,int quantity,int waterQuantity){
		this.type = type;
		this.price = price;
		this.quantity = quantity;
		this.waterQuantity = waterQuantity;
	} 
	
}
