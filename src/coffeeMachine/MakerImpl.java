package coffeeMachine;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class MakerImpl implements Maker {

	private EventsHandler events;
	
	private BeverageQuantity beverageQuantity;
	

	public MakerImpl(BeverageQuantity beverageQuantity) {
		this.events = new EventsHandler();
		this.beverageQuantity = beverageQuantity;
	}
	
	
	@Override
	public String transformer(Order order) {

		if(this.beverageQuantity.isEmpty(order.getDrink().drinkType.name())) {
			this.events.notify(EventsHandler.EVENT_MISSING_DRINK, order.getDrink());
			return "M:{Sorry your " + order.getDrink().drinkType.name() + " cannot be delivred we notify the company for refill .}";
		} 
		
		if(order.getAmount() < order.getDrink().getPrice()) {
			return "M:{the amount is lower than the drink price you need: " + ( BigDecimal.valueOf(order.getDrink().getPrice() - order.getAmount())).setScale(2, RoundingMode.FLOOR) +" }";
		}
		
		return makeDrink(order);
	}
	
	public String makeDrink(Order order) {
		StringBuilder res = new StringBuilder();
		res.append(order.getDrink().getType())
				.append(":")
				.append(order.getSucre() > 0 ? order.getSucre() : "")
				.append(":")
				.append(order.getSucre() > 0 ? "0" : "");
		this.beverageQuantity.provide(order.getDrink().drinkType);
		this.events.notify(EventsHandler.EVENT_REPORT,order.getDrink());
		
		return res.toString();
	}
	
	@Override
	public EventsHandler getEvents() {
		return events;
	}

}
