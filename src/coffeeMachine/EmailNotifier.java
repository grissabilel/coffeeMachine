package coffeeMachine;

public interface EmailNotifier extends  Notifier {
	
	void notifyMissingDrink(String drink);
	
}
