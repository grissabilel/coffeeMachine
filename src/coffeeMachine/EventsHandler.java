package coffeeMachine;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EventsHandler {
	
	public static String EVENT_MISSING_DRINK = "missingDrink";
	public static String EVENT_REPORT = "report";
	
	Map<String,List<Notifier>>  listeners = new HashMap<>();

	public void notify(String eventID ,Drink drink) {
		for(Notifier listener : this.listeners.get(eventID)) {
			if(EVENT_REPORT.equals(eventID))
				((ReportNotifier)listener).notify(drink);
			else if(EVENT_MISSING_DRINK.equals(eventID)) {
				((EmailNotifier)listener).notifyMissingDrink(drink.drinkType.name());	
			}
		}
	}

	public void subscribe(String eventID ,Notifier event) {
		if(!listeners.containsKey(eventID)) {
			listeners.put(eventID, new ArrayList<>());
		}
		listeners.get(eventID).add(event);
	}
	

}
