package coffeeMachine;

public class Application {

	public static void main(String[] args) {

		ReportNotifier report = new Reports();
		EmailNotifier email = new EmailImpl();
		
		BeverageQuantity quantity= new BeverageQuantity();
		//Refill  Machine with resources 
		//Full is considered as 100
		quantity.refill(DrinkType.WATER.type, 10);
		quantity.refill(DrinkType.COFFEE.type, 50);
		quantity.refill(DrinkType.THE.type, 10);
		quantity.refill(DrinkType.CHOCOLATE.type, 20);
		quantity.refill(DrinkType.ORANGE.type, 10);
		
		Maker maker = new MakerImpl(quantity);
		
		maker.getEvents().subscribe(EventsHandler.EVENT_REPORT,report);
		maker.getEvents().subscribe(EventsHandler.EVENT_MISSING_DRINK,email);
		
		System.out.println(maker.transformer(new Order(new DrinkHot(DrinkType.THE), 2, 1f)));
		System.out.println(maker.transformer(new Order(new DrinkHot(DrinkType.COFFEE), 1, 0.2f)));
		System.out.println(maker.transformer(new Order(new DrinkCold(DrinkType.THE), 2, 1f)));
		System.out.println(maker.transformer(new Order(new DrinkCold(DrinkType.COFFEE), 1, 0.8f)));
		System.out.println(maker.transformer(new Order(new DrinkHot(DrinkType.CHOCOLATE), 3, 0.8f)));
		System.out.println(maker.transformer(new Order(new DrinkCold(DrinkType.ORANGE), 0, 1f)));
		
		quantity.print();
		
		report.print();
	}

}
