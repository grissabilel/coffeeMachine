package coffeeMachine;

public abstract class Drink {

	DrinkType drinkType;
	
	Drink(DrinkType drinkType) {
		this.drinkType = drinkType;
	}
	
	public abstract String extra();
	
	public float getPrice() {
		return this.drinkType.price;
	}
	
	public String getType() {
		return this.drinkType.type + this.extra();
	}
}
