package coffeeMachine;

public interface ReportNotifier extends Notifier {
	
	void notify(Drink drink);
	
	void print();

}
