package coffeeMachine;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Instant;
import java.time.ZoneId;
import java.util.HashMap;
import java.util.Map;

public class Reports implements ReportNotifier {
	
	private Map<DrinkType,Integer> drinkBenefints;
	
	public Reports() {
		this.drinkBenefints = new HashMap<>();
	}
	
	@Override
	public void notify(Drink drink) {
		int count = this.drinkBenefints.containsKey(drink.drinkType) ? this.drinkBenefints.get(drink.drinkType) : 0;
		this.drinkBenefints.put(drink.drinkType, count + 1);
	}
	
	@Override 
	public void print() {
		System.out.println("\nReport at "+Instant.now().atZone(ZoneId.of("Europe/Paris")));
		for(Map.Entry<DrinkType, Integer> drink : this.drinkBenefints.entrySet()){
			BigDecimal amount  = BigDecimal.valueOf(drink.getKey().price * drink.getValue()).setScale(2, RoundingMode.FLOOR);
			System.out.println("\n"+drink.getKey().name()+"|"+drink.getValue()+"|"+ amount);
		}
	}

}
