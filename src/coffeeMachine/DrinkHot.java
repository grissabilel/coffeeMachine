package coffeeMachine;

public class DrinkHot extends Drink {

	public DrinkHot(DrinkType drinkType) {
		super(drinkType);
	}

	@Override
	public String extra() {
		return "h";
	}

}
